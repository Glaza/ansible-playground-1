# Recruitment task 

## Requirements

Using anssible:
* install openresty as load balancer
* install openresty + php-fpm as application
* install mysql in master-slave

## Usage

At first run snippet to add public_key to servers 

```bash
ansible-playbook prepare.yml --ask-pass
```

Then run main task

```bash
ansible-playbook playbook.yml
```

## See results

You can see results at https://_load_balancer_ip.nip.io/ e.g. https://198.244.155.43.nip.io/
